<?php

namespace Message;

class TaskStartMessage extends AbstractTaskMessage
{
    /**
     * @var string
     */
    private $configuration;

    /**
     * @var string
     */
    private $destination;

    /**
     * TaskStartMessage constructor.
     * @param $id
     * @param $type
     * @param $configuration
     * @param $destination
     */
    public function __construct($id, $type, $configuration, $destination)
    {
        parent::__construct($id, $type);
        $this->configuration = $configuration;
        $this->destination = $destination;
    }

    /**
     * @return string
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * @return string
     */
    public function getDestination()
    {
        return $this->destination;
    }
}
