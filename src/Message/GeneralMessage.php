<?php

namespace Message;

class GeneralMessage
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var array
     */
    private $parameters;

    /**
     * GeneralMessage constructor
     * @param string $title
     * @param array $parameters
     */
    public function __construct($title, array $parameters = array())
    {
        $this->title = $title;
        $this->parameters = $parameters;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }
}
