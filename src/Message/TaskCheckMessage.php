<?php

namespace Message;

class TaskCheckMessage extends AbstractTaskMessage
{
    /**
     * @var string
     */
    private $configuration;

    /**
     * TaskCheckMessage constructor.
     * @param $id
     * @param $type
     * @param $configuration
     */
    public function __construct($id, $type, $configuration)
    {
        parent::__construct($id, $type);
        $this->configuration = $configuration;
    }

    /**
     * @return string
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }
}
