<?php

namespace Message;

class SimulatorConfigurationMessage
{
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $workerName;

    /**
     * @var string
     */
    private $configuration;

    /**
     * @param string $workerName
     * @param string $configuration
     */
    public function __construct($workerName, $configuration)
    {
        $this->createdAt = new \DateTime('now');
        $this->workerName = $workerName;
        $this->configuration = $configuration;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getWorkerName()
    {
        return $this->workerName;
    }

    /**
     * @return string
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }
}
