<?php

namespace Message;

class UpdateStatusMessage
{
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $workerName;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string|null
     */
    private $comment;

    /**
     * @param string $workerName
     * @param string $status
     * @param string|null $comment
     */
    public function __construct($workerName, $status, $comment)
    {
        $this->createdAt = new \DateTime('now');
        $this->workerName = $workerName;
        $this->status = $status;
        $this->comment = $comment;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getWorkerName()
    {
        return $this->workerName;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }
}
