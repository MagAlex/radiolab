<?php

namespace Message;

class TaskContinueMessage extends AbstractTaskMessage
{
    /**
     * @var string
     */
    private $worker;

    /**
     * @var string
     */
    private $destination;

    public function __construct($worker, $id, $type, $destination)
    {
        parent::__construct($id, $type);
        $this->worker = $worker;
        $this->destination = $destination;
    }

    /**
     * @return string
     */
    public function getWorker()
    {
        return $this->worker;
    }

    /**
     * @return string
     */
    public function getDestination()
    {
        return $this->destination;
    }
}
