<?php

namespace Message;

class TaskStatusMessage
{
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $workerName;

    /**
     * @var int
     */
    private $taskId;

    /**
     * @var string
     */
    private $status;

    /**
     * @var int|null
     */
    private $progress;

    /**
     * @var string|null
     */
    private $comment;

    /**
     * @param string $workerName
     * @param int $taskId
     * @param string $status
     * @param int|null $progress
     * @param string|null $comment
     */
    public function __construct($workerName, $taskId, $status, $progress, $comment)
    {
        $this->createdAt = new \DateTime('now');
        $this->workerName = $workerName;
        $this->taskId = $taskId;
        $this->status = $status;
        $this->progress = $progress;
        $this->comment = $comment;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getWorkerName()
    {
        return $this->workerName;
    }

    /**
     * @return int
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return int|null
     */
    public function getProgress()
    {
        return $this->progress;
    }

    /**
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }
}
