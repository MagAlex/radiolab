<?php

namespace Configuration;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class LoggerConfiguration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('logger');

        $rootNode
            ->children()
                ->scalarNode('task_logger')->end()
                ->scalarNode('update_logger')->end()
                ->scalarNode('type_logger')->end()
            ->end();

        return $treeBuilder;
    }
}
