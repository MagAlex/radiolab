<?php

namespace Service;

use Configuration\RabbitMQConfiguration;
use Message\GeneralMessage;
use Message\SimulatorConfigurationMessage;
use Message\TaskCheckMessage;
use Message\TaskStartMessage;
use Message\TaskStatusMessage;
use Message\UpdateStatusMessage;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Yaml\Yaml;

class RabbitMQService
{
    /**
     * @var AMQPStreamConnection
     */
    private $connection;

    /**
     * @var string
     */
    private $selfQueueName;

    public function __construct()
    {
        $configuration = $this->loadConfiguration();

        $client = $configuration['client'];
        $this->selfQueueName = 'worker-'.$client['id'];

        $server = $configuration['server'];
        $this->connection = new AMQPStreamConnection($server['host'], $server['port'], $server['user'], $server['password']);
    }

    public function __destruct()
    {
        $this->connection->close();
    }

    /**
     * @return string
     */
    public function getSelfQueueName()
    {
        return $this->selfQueueName;
    }

    /**
     * @return AMQPChannel
     */
    public function createChannel()
    {
        return $this->connection->channel();
    }

    /**
     * @return AMQPChannel
     */
    public function createTaskChannel()
    {
        $channel = $this->createChannel();
        $channel->queue_declare('task', false, true, false, false);
        $channel->basic_qos(0, 1, false);

        return $channel;
    }

    /**
     * @return AMQPChannel
     */
    public function createTaskProgressChannel()
    {
        $channel = $this->createChannel();
        $channel->queue_declare('progress', false, true, false, false);
        $channel->basic_qos(0, 1, false);

        return $channel;
    }

    /**
     * @return AMQPChannel
     */
    public function createGeneralChannel()
    {
        $channel = $this->createExchangeChannel('general');
        $channel->queue_declare($this->selfQueueName, false, true, false, false);
        $channel->basic_qos(0, 0, false);
        $channel->queue_bind($this->selfQueueName, 'general');

        return $channel;
    }

    /**
     * @param TaskStartMessage $task
     */
    public function sendTaskStartMessage(TaskStartMessage $task)
    {
        $message = new AMQPMessage(serialize($task));
        $channel = $this->createProduceChannel('task');
        $channel->basic_publish($message, '', 'task');
        $channel->close();
    }

    /**
     * @param TaskCheckMessage $task
     */
    public function sendTaskCheckMessage(TaskCheckMessage $task)
    {
        $message = new AMQPMessage(serialize($task));
        $channel = $this->createProduceChannel('task');
        $channel->basic_publish($message, '', 'task');
        $channel->close();
    }

    /**
     * @param TaskStatusMessage $log
     */
    public function sendTaskStatusMessage(TaskStatusMessage $log)
    {
        $message = new AMQPMessage(serialize($log));
        $channel = $this->createProduceChannel('progress');
        $channel->basic_publish($message, '', 'progress');
        $channel->close();
    }

    /**
     * @param UpdateStatusMessage $log
     */
    public function sendUpdateStatusMessage(UpdateStatusMessage $log)
    {
        $message = new AMQPMessage(serialize($log));
        $channel = $this->createProduceChannel('progress');
        $channel->basic_publish($message, '', 'progress');
        $channel->close();
    }

    /**
     * @param SimulatorConfigurationMessage $log
     */
    public function sendSimulatorConfigurationMessage(SimulatorConfigurationMessage $log)
    {
        $message = new AMQPMessage(serialize($log));
        $channel = $this->createProduceChannel('progress');
        $channel->basic_publish($message, '', 'progress');
        $channel->close();
    }

    /**
     * @param GeneralMessage $generalMessage
     * @param string|null $queue
     */
    public function sendGeneralMessage(GeneralMessage $generalMessage, $queue = null)
    {
        $message = new AMQPMessage(serialize($generalMessage));

        if ($queue) {
            $channel = $this->createProduceChannel($queue);
            $channel->basic_publish($message, '', $queue);
            $channel->close();
        } else {
            $channel = $this->createExchangeChannel('general');
            $channel->basic_publish($message, 'general');
            $channel->close();
        }
    }

    /**
     * @param AMQPMessage $message
     */
    public function confirmMessage(AMQPMessage $message)
    {
        /** @var AMQPChannel $channel */
        $channel = $message->delivery_info['channel'];
        $channel->basic_ack($message->delivery_info['delivery_tag']);
    }

    /**
     * @return array
     */
    private function loadConfiguration()
    {
        $config = Yaml::parse(file_get_contents(__DIR__.'/../../config/rabbitmq.yaml'));
        $processor = new Processor();
        $rabbitMQConfiguration = new RabbitMQConfiguration();

        return $processor->processConfiguration($rabbitMQConfiguration, $config);
    }

    /**
     * @param string $queue
     * @return AMQPChannel
     */
    private function createProduceChannel($queue)
    {
        $channel = $this->createChannel();
        $channel->queue_declare($queue, false, true, false, false);

        return $channel;
    }

    /**
     * @param string $exchange
     * @return AMQPChannel
     */
    private function createExchangeChannel($exchange)
    {
        $channel = $this->createChannel();
        $channel->exchange_declare($exchange, 'fanout', false, true, false);

        return $channel;
    }
}
