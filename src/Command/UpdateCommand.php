<?php

namespace Command;

use Message\GeneralMessage;
use Service\RabbitMQService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('update')
            ->setDescription('Отправка задачи обновления расчётного модуля')
            ->addArgument('type', InputArgument::REQUIRED, 'Тип задачи')
            ->addArgument('archive', InputArgument::REQUIRED, 'Zip-архив модуля');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $type = $input->getArgument('type');
        $archive = $input->getArgument('archive');

        $generalMessage = new GeneralMessage(
            'update',
            array(
                'type' => $type,
                'archive' => $archive,
            )
        );

        $rabbit = new RabbitMQService();
        $rabbit->sendGeneralMessage($generalMessage);
    }
}
