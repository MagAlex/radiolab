<?php

namespace Command;

use Message\AbstractTaskMessage;
use Message\GeneralMessage;
use PhpAmqpLib\Message\AMQPMessage;
use Service\RabbitMQService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TaskStopCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('task:stop')
            ->setDescription('Остановка выполнения задачи')
            ->addArgument('id', InputArgument::REQUIRED, 'Идентификатор задачи');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getArgument('id');

        $generalMessage = new GeneralMessage('stop', array('id' => $id));

        $rabbit = new RabbitMQService();
        $rabbit->sendGeneralMessage($generalMessage);

        $taskChannel = $rabbit->createTaskChannel();

        $messages = array();
        while (true) {
            $message = $taskChannel->basic_get('task');
            if (!$message) {
                break;
            }

            $messages[] = $message;
        }

        /** @var AMQPMessage $message */
        foreach ($messages as $message) {
            /** @var AbstractTaskMessage $taskMessage */
            $taskMessage = unserialize($message->getBody());
            if ($taskMessage->getId() === $id) {
                $taskChannel->basic_reject($message->delivery_info['delivery_tag'], false);
            } else {
                $taskChannel->basic_reject($message->delivery_info['delivery_tag'], true);
            }
        }
    }
}
