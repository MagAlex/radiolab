<?php

namespace Command;

use Message\TaskStartMessage;
use Service\RabbitMQService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TaskStartCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('task:start')
            ->setDescription('Отправка задачи в очередь на исполнение')
            ->addArgument('id', InputArgument::REQUIRED, 'Идентификатор задачи')
            ->addArgument('type', InputArgument::REQUIRED, 'Тип задачи')
            ->addArgument('configuration', InputArgument::REQUIRED, 'Файл конфигурации')
            ->addArgument('destination', InputArgument::REQUIRED, 'Полный сетевой адрес, куда будут загружены результаты выполнения задачи');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getArgument('id');
        $type = $input->getArgument('type');

        $configurationFile = $input->getArgument('configuration');
        if (!file_exists($configurationFile)) {
            throw new \Exception(sprintf("File '%s' not found", $configurationFile));
        }
        $configuration = file_get_contents($configurationFile);

        $destination = $input->getArgument('destination');

        $taskStartMessage = new TaskStartMessage($id, $type, $configuration, $destination);

        $rabbit = new RabbitMQService();
        $rabbit->sendTaskStartMessage($taskStartMessage);
    }
}
