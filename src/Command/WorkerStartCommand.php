<?php

namespace Command;

use Configuration\SimulatorConfiguration;
use Message\GeneralMessage;
use Message\SimulatorConfigurationMessage;
use Message\TaskCheckMessage;
use Message\TaskContinueMessage;
use Message\TaskStartMessage;
use Message\TaskStatusMessage;
use Message\UpdateStatusMessage;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;
use Service\RabbitMQService;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Yaml\Yaml;

class WorkerStartCommand extends Command
{
    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var RabbitMQService
     */
    private $rabbit;

    /**
     * @var string
     */
    private $selfQueueName;

    /**
     * @var AMQPChannel
     */
    private $generalChannel;

    /**
     * @var GeneralMessage
     */
    private $generalMessages = array();

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('worker:start')
            ->setDescription('Запуск демона отвечающего за выполнение расчётных задач');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;

        while (true) {
            try {
                $this->rabbit = new RabbitMQService();
                $this->generalChannel = $this->rabbit->createGeneralChannel();
                $taskChannel = $this->rabbit->createTaskChannel();

                $this->selfQueueName = $this->rabbit->getSelfQueueName();
                $this->output->writeln(sprintf('Worker \'%s\' started', $this->selfQueueName));

                while (true) {
                    $this->handleGeneralMessages();

                    /** @var GeneralMessage $generalMessage */
                    foreach ($this->generalMessages as $generalMessage) {
                        switch ($generalMessage->getTitle()) {
                            case 'type:list':
                                $this->sendSimulatorConfiguration();
                                break;

                            case 'type:set':
                                $parameters = $generalMessage->getParameters();
                                $this->setSimulatorConfiguration($parameters['config']);
                                break;

                            case 'update':
                                $this->updateSimulator($generalMessage->getParameters());
                                break;
                        }
                    }
                    $this->generalMessages = array();

                    usleep(100000);

                    /** @var AMQPMessage $message */
                    $message = $taskChannel->basic_get('task');
                    if ($message) {
                        $taskChannel->basic_ack($message->delivery_info['delivery_tag']);
                        $taskMessage = unserialize($message->getBody());
                        if ($taskMessage instanceof TaskStartMessage) {
                            $this->taskStart($taskMessage);
                        } elseif ($taskMessage instanceof TaskContinueMessage) {
                            $this->taskContinue($taskMessage);
                        } elseif ($taskMessage instanceof TaskCheckMessage) {
                            $this->checkConfiguration($taskMessage);
                        }
                    }

                    usleep(100000);
                }

                $taskChannel->close();
                $this->generalChannel->close();
            } catch (\Exception $exception) {
                $this->output->writeln(' <bg=red;fg=white> EXCEPTION </> '.$exception->getMessage());
            }

            $this->output->writeln('Wait 60 seconds...');
            sleep(60);
        }
    }

    /**
     * @param int $taskId
     * @param Process|null $process
     * @return string
     */
    private function handleGeneralMessages($taskId = 0, Process $process = null)
    {
        /** @var AMQPMessage $message */
        $message = $this->generalChannel->basic_get($this->selfQueueName);
        if ($message) {
            $this->generalChannel->basic_ack($message->delivery_info['delivery_tag']);

            /** @var GeneralMessage $generalMessage */
            $generalMessage = unserialize($message->getBody());
            switch ($generalMessage->getTitle()) {
                case 'type:list':
                case 'type:set':
                case 'update':
                    $this->generalMessages[] = $generalMessage;

                    return $generalMessage->getTitle();
                    break;

                case 'stop':
                    if ($this->taskStop($generalMessage->getParameters(), $taskId, $process)) {
                        return $generalMessage->getTitle();
                    }
                    break;
            }
        }

        return '';
    }

    /**
     * @param TaskStartMessage $task
     */
    private function taskStart(TaskStartMessage $task)
    {
        $this->output->writeln(sprintf('Start task type=\'%s\' id=\'%s\'', $task->getType(), $task->getId()));

        $simulatorConfiguration = $this->loadSimulatorConfiguration();

        if (!isset($simulatorConfiguration['types'][$task->getType()])) {
            $comment = sprintf('Simulator type \'%s\' not supported', $task->getType());
            $this->output->writeln(' <bg=red;fg=white> FAIL </> '.$comment);

            $taskStatusMessage = new TaskStatusMessage($this->selfQueueName, $task->getId(), 'fail', null, $comment);
            $this->rabbit->sendTaskStatusMessage($taskStatusMessage);

            return;
        }

        $simulator = $simulatorConfiguration['types'][$task->getType()];
        $simulator['path'] = $simulatorConfiguration['root_path'].'/'.$task->getType();

        chdir($simulator['path']);

        $taskDirectory = sprintf('config/%s', $task->getId());
        if (!is_dir($taskDirectory)) {
            mkdir($taskDirectory, 0775, true);
        }

        $configurationFile = sprintf('%s/config.m', $taskDirectory);
        file_put_contents($configurationFile, $task->getConfiguration());

        $resultDirectory = sprintf('results/config/%s/config.m', $task->getId());

        $progressFile = sprintf('%s/progress.txt', $resultDirectory);
        if (file_exists($progressFile)) {
            unlink($progressFile);
        }

        $this->taskRun($simulator, $task->getId(), $configurationFile, $resultDirectory, $progressFile, $task->getDestination());
    }

    /**
     * @param TaskContinueMessage $task
     */
    private function taskContinue(TaskContinueMessage $task)
    {
        $this->output->writeln(sprintf('Continue task type=\'%s\' id=\'%s\'', $task->getType(), $task->getId()));

        $simulatorConfiguration = $this->loadSimulatorConfiguration();

        if (!isset($simulatorConfiguration['types'][$task->getType()])) {
            $comment = sprintf('Simulator type \'%s\' not supported', $task->getType());
            $this->output->writeln(' <bg=red;fg=white> FAIL </> '.$comment);

            $taskStatusMessage = new TaskStatusMessage($this->selfQueueName, $task->getId(), 'fail', null, $comment);
            $this->rabbit->sendTaskStatusMessage($taskStatusMessage);

            return;
        }

        $simulator = $simulatorConfiguration['types'][$task->getType()];
        $simulator['path'] = $simulatorConfiguration['root_path'].'/'.$task->getType();

        $configurationFile = sprintf('config/%s/config.m', $task->getId());
        $resultDirectory = sprintf('results/config/%s/config.m', $task->getId());
        $progressFile = sprintf('%s/progress.txt', $resultDirectory);

        $this->taskRun($simulator, $task->getId(), $configurationFile, $resultDirectory, $progressFile, $task->getDestination());
    }

    /**
     * @param array $simulator
     * @param int $taskId
     * @param string $configurationFile
     * @param string $resultDirectory
     * @param string $progressFile
     * @param string $resultDestination
     */
    private function taskRun(array $simulator, $taskId, $configurationFile, $resultDirectory, $progressFile, $resultDestination)
    {
        $taskStatusMessage = new TaskStatusMessage($this->selfQueueName, $taskId, 'started', null, null);
        $this->rabbit->sendTaskStatusMessage($taskStatusMessage);

        $command = sprintf('./%s %s %s', $simulator['exe'], $simulator['params'], $configurationFile);
        $this->output->writeln(sprintf(' <bg=blue;fg=white> RUN </> <fg=blue>%s</>', $command));

        $process = new Process($command);
        $process->setEnhanceSigchildCompatibility(true);
        if ($this->output->getVerbosity() <= OutputInterface::VERBOSITY_NORMAL) {
            $process->disableOutput();
        }
        $process->start();

        $pid = $process->getPid();
        $this->output->writeln(sprintf('Running process %d', $pid));

        $isStopped = false;
        $isZipped = false;

        while ($process->isRunning()) {
            if (file_exists($progressFile)) {
                $this->output->writeln(sprintf('Progress file \'%s\'', $progressFile));
                break;
            }

            usleep(100000);

            $messageTitle = $this->handleGeneralMessages($taskId, $process);
            if ($messageTitle === 'stop') {
                $isStopped = true;
            }

            usleep(100000);
        }

        if ($process->isRunning()) {
            $progress = 0;
            while ($process->isRunning()) {
                $newProgress = intval(file_get_contents($progressFile));
                if ($newProgress > 0 && $newProgress != $progress) {
                    $progress = $newProgress;

                    $this->output->write("\x0D");
                    $this->output->write(sprintf('Progress %3s%%', $progress));

                    $taskStatusMessage = new TaskStatusMessage($this->selfQueueName, $taskId, 'running', $progress, null);
                    $this->rabbit->sendTaskStatusMessage($taskStatusMessage);
                }

                usleep(100000);

                $messageTitle = $this->handleGeneralMessages($taskId, $process);
                if ($messageTitle === 'stop') {
                    $isStopped = true;
                }

                usleep(100000);
            }

            if (file_exists($progressFile)) {
                $newProgress = intval(file_get_contents($progressFile));
                if ($newProgress > 0 && $newProgress != $progress) {
                    $progress = $newProgress;

                    $this->output->write("\x0D");
                    $this->output->write(sprintf('Progress %3s%%', $progress));

                    $taskStatusMessage = new TaskStatusMessage($this->selfQueueName, $taskId, 'running', $progress, null);
                    $this->rabbit->sendTaskStatusMessage($taskStatusMessage);
                }
            }

            $this->output->writeln('');

            if (!$isStopped) {
                chdir($resultDirectory);

                $this->output->writeln('Zipping results');
                $zipFileName = sprintf('%s-task-%s.zip', $this->selfQueueName, $taskId);
                $command = sprintf('zip -r %s .', $zipFileName);
                system($command);

                $this->output->writeln(sprintf('Sending results to %s', $resultDestination));
                if (filter_var($resultDestination, FILTER_VALIDATE_URL)) {
                    if (version_compare(PHP_VERSION, '5.5') >= 0) {
                        $zipFileRealPath = realpath($zipFileName);
                        $zipFileMimeType = mime_content_type($zipFileRealPath);
                        $postFields = array('file' => new \CURLFile($zipFileRealPath, $zipFileMimeType, $zipFileName));
                    } else {
                        $postFields = array('file' => '@'.realpath($zipFileName));
                    }
                    $ch = curl_init($resultDestination);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data'));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
                    $response = curl_exec($ch);
                    curl_close($ch);

                    if ($this->output->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
                        echo $response.PHP_EOL;
                    }
                }

                chdir($simulator['path']);

                $isZipped = true;
            }
        }

        if ($this->output->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
            echo $process->getOutput().PHP_EOL;
        }
        if ($this->output->getVerbosity() >= OutputInterface::VERBOSITY_DEBUG) {
            echo $process->getErrorOutput().PHP_EOL;
        }

        if ($isStopped) {
            $taskStatusMessage = new TaskStatusMessage($this->selfQueueName, $taskId, 'stopped', null, null);
            $this->rabbit->sendTaskStatusMessage($taskStatusMessage);
            $this->output->writeln(' <bg=red;fg=white> STOPPED </>');

            return;
        }

        //$failComment = 'Unknown error';

        if ($process->getExitCode() === 0) {
            if ($isZipped || file_exists($progressFile)) {
                $progress = intval(file_get_contents($progressFile));
                if ($isZipped || $progress === 100) {
                    $taskStatusMessage = new TaskStatusMessage($this->selfQueueName, $taskId, 'done', null, null);
                    $this->rabbit->sendTaskStatusMessage($taskStatusMessage);
                    $this->output->writeln(' <bg=green;fg=white> DONE </>');

                    return;
                } else {
                    $failComment = 'Progress value = '.$progress;
                }
            } else {
                $failComment = 'Progress file does not exists';
            }
        } else {
            $failComment = 'Process return exit code = '.$process->getExitCode();
        }

        $taskStatusMessage = new TaskStatusMessage($this->selfQueueName, $taskId, 'fail', null, $failComment);
        $this->rabbit->sendTaskStatusMessage($taskStatusMessage);
        $this->output->writeln(' <bg=red;fg=white> FAIL </> '.$failComment);
    }

    /**
     * @param TaskCheckMessage $task
     */
    private function checkConfiguration(TaskCheckMessage $task)
    {
        $this->output->writeln(sprintf('Check type=\'%s\' id=\'%s\'', $task->getType(), $task->getId()));

        $simulatorConfiguration = $this->loadSimulatorConfiguration();

        if (!isset($simulatorConfiguration['types'][$task->getType()])) {
            $this->output->writeln(sprintf(' <bg=red;fg=white> FAIL </> Simulator type \'%s\' not supported', $task->getType()));

            return;
        }

        $this->output->writeln(' <bg=red;fg=white> FAIL </> Check not implemented!');
    }

    /**
     * @param array $parameters
     */
    private function updateSimulator(array $parameters)
    {
        $type = $parameters['type'];
        $remoteArchive = $parameters['archive'];
        $this->output->writeln(sprintf(' <bg=blue;fg=white> UPDATE </> <fg=blue>%s %s</>', $type, $remoteArchive));

        if (!filter_var($remoteArchive, FILTER_VALIDATE_URL)) {
            $comment = sprintf('Invalid url \'%s\'', $remoteArchive);
            $this->output->writeln(' <bg=red;fg=white> FAIL </> '.$comment);

            $updateStatusMessage = new UpdateStatusMessage($this->selfQueueName, 'fail', $comment);
            $this->rabbit->sendUpdateStatusMessage($updateStatusMessage);

            return;
        }

        $simulatorConfiguration = $this->loadSimulatorConfiguration();

        if (!isset($simulatorConfiguration['types'][$type])) {
            $comment = sprintf('Simulator type \'%s\' not supported', $type);
            $this->output->writeln(' <bg=red;fg=white> FAIL </> '.$comment);

            $updateStatusMessage = new UpdateStatusMessage($this->selfQueueName, 'fail', $comment);
            $this->rabbit->sendUpdateStatusMessage($updateStatusMessage);

            return;
        }

        $simulator = $simulatorConfiguration['types'][$type];
        $simulator['path'] = $simulatorConfiguration['root_path'].'/'.$type;

        if (!is_dir($simulator['path'])) {
            mkdir($simulator['path'], 0777, true);
        }
        chdir($simulator['path']);

        $this->output->writeln(sprintf('Download from %s', $remoteArchive));

        $localArchive = basename($remoteArchive);
        copy($remoteArchive, $localArchive);

        if (!file_exists($localArchive)) {
            $comment = sprintf('File not downloaded \'%s\'', $remoteArchive);
            $this->output->writeln(' <bg=red;fg=white> FAIL </> '.$comment);

            $updateStatusMessage = new UpdateStatusMessage($this->selfQueueName, 'fail', $comment);
            $this->rabbit->sendUpdateStatusMessage($updateStatusMessage);
        }

        $this->output->writeln(sprintf('Extract to %s', $simulator['path']));

        $zip = new \ZipArchive();
        if (!$zip->open($localArchive)) {
            $comment = sprintf('Bad archive');
            $this->output->writeln(' <bg=red;fg=white> FAIL </> '.$comment);

            $updateStatusMessage = new UpdateStatusMessage($this->selfQueueName, 'fail', $comment);
            $this->rabbit->sendUpdateStatusMessage($updateStatusMessage);

            return;
        }

        $zip->extractTo('.');
        $zip->close();
        unlink($localArchive);

        $shells = glob('*.sh');
        foreach ($shells as $shell) {
            chmod($shell, 0775);
        }

        $this->output->writeln(' <bg=green;fg=white> DONE </>');
        $updateStatusMessage = new UpdateStatusMessage($this->selfQueueName, 'done', null);
        $this->rabbit->sendUpdateStatusMessage($updateStatusMessage);
    }

    /**
     * @param array $parameters
     * @param int $taskId
     * @param Process|null $process
     * @return bool
     */
    private function taskStop(array $parameters, $taskId = 0, Process $process = null)
    {
        if ($taskId && $process) {
            if ($parameters['id'] == $taskId) {
                exec('pkill -'.SIGTERM.' -P '.$process->getPid());

                return true;
            }
        }

        return false;
    }

    private function sendSimulatorConfiguration()
    {
        $simulatorConfiguration = $this->loadSimulatorConfiguration();
        $simulatorConfigurationMessage = new SimulatorConfigurationMessage($this->selfQueueName, serialize($simulatorConfiguration['types']));
        $this->rabbit->sendSimulatorConfigurationMessage($simulatorConfigurationMessage);
    }

    /**
     * @param string $config
     */
    private function setSimulatorConfiguration($config)
    {
        try {
            $currentConfig = $this->loadSimulatorConfiguration();
            $newConfig = array(
                'simulator' => array(
                    'root_path' => $currentConfig['root_path'],
                    'types' => unserialize($config),
                ),
            );

            $processor = new Processor();
            $simulatorConfiguration = new SimulatorConfiguration();
            $processor->processConfiguration($simulatorConfiguration, $newConfig);

            $content = Yaml::dump($newConfig, 4, 2);
            file_put_contents(__DIR__.'/../../config/simulator.yaml', $content);
        } catch (\Exception $exception) {
            $this->output->writeln(' <bg=red;fg=white> EXCEPTION </> '.$exception->getMessage());
        }

        $this->sendSimulatorConfiguration();
    }

    /**
     * @return array
     */
    private function loadSimulatorConfiguration()
    {
        $config = Yaml::parse(file_get_contents(__DIR__.'/../../config/simulator.yaml'));

        $processor = new Processor();
        $simulatorConfiguration = new SimulatorConfiguration();

        return $processor->processConfiguration($simulatorConfiguration, $config);
    }
}
