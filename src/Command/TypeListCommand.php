<?php

namespace Command;

use Message\GeneralMessage;
use Service\RabbitMQService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TypeListCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('type:list')
            ->setDescription('Получает список поддерживаемых типов задач');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $generalMessage = new GeneralMessage('type:list');

        $rabbit = new RabbitMQService();
        $rabbit->sendGeneralMessage($generalMessage);
    }
}
