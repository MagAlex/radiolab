<?php

namespace Command;

use Message\AbstractTaskMessage;
use PhpAmqpLib\Message\AMQPMessage;
use Service\RabbitMQService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class QueueShowTasksCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('queue:show-tasks')
            ->setDescription('Получает список задач в очереди');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $rabbit = new RabbitMQService();
        $taskChannel = $rabbit->createTaskChannel();

        $messages = array();
        $taskMessages = array();

        while (true) {
            $message = $taskChannel->basic_get('task');
            if (!$message) {
                break;
            }

            $messages[] = $message;

            /** @var AbstractTaskMessage $taskMessage */
            $taskMessage = unserialize($message->getBody());
            $taskMessages[] = $taskMessage->getId();
        }

        /** @var AMQPMessage $message */
        foreach ($messages as $message) {
            $taskChannel->basic_reject($message->delivery_info['delivery_tag'], true);
        }

        echo json_encode($taskMessages).PHP_EOL;
    }
}
