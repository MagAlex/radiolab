<?php

namespace Command;

use Message\GeneralMessage;
use Service\RabbitMQService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TypeSetCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('type:set')
            ->setDescription('Задаёт список поддерживаемых типов задач')
            ->addArgument('config', InputArgument::REQUIRED, 'Конфигурация');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $config = $input->getArgument('config');

        $generalMessage = new GeneralMessage(
            'type:set',
            array(
                'config' => $config,
            )
        );

        $rabbit = new RabbitMQService();
        $rabbit->sendGeneralMessage($generalMessage);
    }
}
