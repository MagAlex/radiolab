<?php

namespace Command;

use Configuration\LoggerConfiguration;
use Message\SimulatorConfigurationMessage;
use Message\TaskStatusMessage;
use Message\UpdateStatusMessage;
use PhpAmqpLib\Message\AMQPMessage;
use Service\RabbitMQService;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

class LoggerStartCommand extends Command
{
    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var array
     */
    private $loggerConfiguration;

    /**
     * @var RabbitMQService
     */
    private $rabbit;

    protected function configure()
    {
        $this
            ->setName('logger:start')
            ->setDescription('Запуск демона принимающего логи');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->loggerConfiguration = $this->loadConfiguration();

        while (true) {
            try {
                $this->rabbit = new RabbitMQService();
                $channel = $this->rabbit->createTaskProgressChannel();

                $this->output->writeln('Logger started');

                while (true) {
                    /** @var AMQPMessage $message */
                    $message = $channel->basic_get('progress');

                    if ($message) {
                        $channel->basic_ack($message->delivery_info['delivery_tag']);
                        $this->log($message);
                    }

                    usleep(100000);
                }

                $channel->close();
            } catch (\Exception $exception) {
                $this->output->writeln(' <bg=red;fg=white> EXCEPTION </> '.$exception->getMessage());
            }

            $this->output->writeln('Wait 60 seconds...');
            sleep(60);
        }
    }

    /**
     * @param AMQPMessage $message
     */
    public function log(AMQPMessage $message)
    {
        $statusMessage = unserialize($message->getBody());

        if ($statusMessage instanceof TaskStatusMessage) {
            $taskLogger = $this->loggerConfiguration['task_logger'];
            chdir(dirname($taskLogger));

            $createdAt = $statusMessage->getCreatedAt()->format(\DateTime::ATOM);
            $workerName = $statusMessage->getWorkerName();
            $taskId = $statusMessage->getTaskId();
            $status = $statusMessage->getStatus();
            $progress = $statusMessage->getProgress();
            $comment = $statusMessage->getComment();

            if ($progress) {
                $this->output->writeln(sprintf('%s, Worker: %s, Task %s; Status: %s; Progress %s%%', $createdAt, $workerName, $taskId, $status, $progress));
                exec(sprintf('php %s %s %s %s %s %s', $taskLogger, $createdAt, $workerName, $taskId, $status, $progress));
            } else {
                if ($comment) {
                    $this->output->writeln(sprintf('%s, Worker: %s, Task %s; Status: %s; Comment: %s', $createdAt, $workerName, $taskId, $status, $comment));
                    exec(sprintf('php %s %s %s %s %s "%s"', $taskLogger, $createdAt, $workerName, $taskId, $status, $comment));
                } else {
                    $this->output->writeln(sprintf('%s, Worker: %s, Task %s; Status: %s', $createdAt, $workerName, $taskId, $status));
                    exec(sprintf('php %s %s %s %s %s', $taskLogger, $createdAt, $workerName, $taskId, $status));
                }
            }
        } elseif ($statusMessage instanceof UpdateStatusMessage) {
            $updateLogger = $this->loggerConfiguration['update_logger'];
            chdir(dirname($updateLogger));

            $createdAt = $statusMessage->getCreatedAt()->format(\DateTime::ATOM);
            $workerName = $statusMessage->getWorkerName();
            $status = $statusMessage->getStatus();
            $comment = $statusMessage->getComment();

            if ($comment) {
                $this->output->writeln(sprintf('%s, Worker: %s, Status: %s; Comment: %s', $createdAt, $workerName, $status, $comment));
                exec(sprintf('php %s %s %s %s "%s"', $updateLogger, $createdAt, $workerName, $status, $comment));
            } else {
                $this->output->writeln(sprintf('%s, Worker: %s, Status: %s', $createdAt, $workerName, $status));
                exec(sprintf('php %s %s %s %s', $updateLogger, $createdAt, $workerName, $status));
            }
        } elseif ($statusMessage instanceof SimulatorConfigurationMessage) {
            $typeLogger = $this->loggerConfiguration['type_logger'];
            chdir(dirname($typeLogger));

            $createdAt = $statusMessage->getCreatedAt()->format(\DateTime::ATOM);
            $workerName = $statusMessage->getWorkerName();
            $configuration = $statusMessage->getConfiguration();

            $this->output->writeln(sprintf('%s, Worker: %s, Simulator Configuration: %s', $createdAt, $workerName, $configuration));
            exec(sprintf('php %s %s %s \'%s\'', $typeLogger, $createdAt, $workerName, $configuration));
        }
    }

    /**
     * @return array
     */
    private function loadConfiguration()
    {
        $config = Yaml::parse(file_get_contents(__DIR__.'/../../config/logger.yaml'));
        $processor = new Processor();
        $loggerConfiguration = new LoggerConfiguration();

        return $processor->processConfiguration($loggerConfiguration, $config);
    }
}
