<?php

namespace Command;

use Message\GeneralMessage;
use Message\TaskContinueMessage;
use Service\RabbitMQService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TaskContinueCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('task:continue')
            ->setDescription('Продолжение выполнения задачи')
            ->addArgument('worker', InputArgument::REQUIRED, 'Имя сервера, который ранее выполнял остановленную задачу')
            ->addArgument('id', InputArgument::REQUIRED, 'Идентификатор задачи')
            ->addArgument('type', InputArgument::REQUIRED, 'Тип задачи')
            ->addArgument('destination', InputArgument::REQUIRED, 'Полный сетевой адрес, куда будут загружены результаты выполнения задачи');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $worker = $input->getArgument('worker');
        $id = $input->getArgument('id');
        $type = $input->getArgument('type');
        $destination = $input->getArgument('destination');

        $taskContinueMessage = new TaskContinueMessage($worker, $id, $type, $destination);

        $message = new GeneralMessage('continue', array('task' => $taskContinueMessage));

        $rabbit = new RabbitMQService();
        $rabbit->sendGeneralMessage($message, $worker);
    }
}
