<?php

namespace Command;

use Message\TaskCheckMessage;
use Service\RabbitMQService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TaskCheckCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('task:check')
            ->setDescription('Проверка файла конфигурации')
            ->addArgument('id', InputArgument::REQUIRED, 'Идентификатор задачи')
            ->addArgument('type', InputArgument::REQUIRED, 'Тип задачи')
            ->addArgument('configuration', InputArgument::REQUIRED, 'Файл конфигурации');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getArgument('id');
        $type = $input->getArgument('type');

        $configurationFile = $input->getArgument('configuration');
        if (!file_exists($configurationFile)) {
            throw new \Exception(sprintf("File '%s' not found", $configurationFile));
        }
        $configuration = file_get_contents($configurationFile);

        $taskCheckMessage = new TaskCheckMessage($id, $type, $configuration);

        $rabbit = new RabbitMQService();
        $rabbit->sendTaskCheckMessage($taskCheckMessage);
    }
}
